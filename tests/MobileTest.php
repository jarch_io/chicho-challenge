<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;

use App\Mobile;
use App\Carrier\Local;

class MobileTest extends TestCase
{	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = new Local();
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function call_by_name()
	{
		$provider = new Local();
		$mobile = new Mobile($provider);

		$this->assertIsObject($mobile->makeCallByName('Jose'));
	}

	/** @test */
	public function verify_contact_by_name()
	{
		$provider = new Local();
		$mobile = new Mobile($provider);

		$this->assertIsBool($mobile->validateExists('Jose'));
	}

	/** @test */
	public function local_provider_successfully_sms()
	{
		$provider = new Local();
		$mobile = new Mobile($provider);

		$this->assertSame(true, $mobile->sendSMS('+51950119887', 'Hola Mundo'));
	}

	/** @test */
	public function local_provider_error_sms()
	{
		$provider = new Local();
		$mobile = new Mobile($provider);

		$this->assertSame(false, $mobile->sendSMS('+519501198877', 'Hola Mundo'));
	}

}
