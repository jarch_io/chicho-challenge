<?php
namespace App\Carrier;

use App\Interfaces\CarrierInterface;

use App\Contact;
use App\Call;

class Local implements CarrierInterface
{
    
    public function __construct()
    {
        
    }

    public function dialContact(Contact $contact)
    {
    	
    }

    public function makeCall(): Call
    {
    	return new Call;
    }

    public function sendSms(string $number, string $message): bool
    {
        echo "send to: {$number} : {$message}";
        return true;
    }
}