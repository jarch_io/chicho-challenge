<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}

	public function validateExists($name = '')
	{
		if( empty($name) ) return false;

		return ContactService::existsByName($name);
	}


	public function sendSMS(string $number, string $message)
	{
		if(empty($number) || empty($message)) return false;

		$isValidNumber = ContactService::validateNumber($number);

		if(!$isValidNumber) return false;

		return $this->provider->sendSms($number, $message);
	}
	
}
