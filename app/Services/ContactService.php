<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	
	public static function findByName(string $name): Contact
	{
		$contacModel = null;
		foreach (self::_getContacts() as $contact) {
			if($contact['name'] == $name) {
				$contacModel = new Contact;
				$contacModel->name = $contact['name'];
				$contacModel->number = $contact['number'];
				break;
			}
		}
		return $contacModel;
	}

	public static function existsByName(string $name): bool
	{
		return !is_null(self::findByName($name));
	}

	public static function validateNumber(string $number): bool
	{
		return preg_match('/^\+51\d{9}$/', $number) === 1;
	}

	private static function _getContacts()
	{
		return array(
			array(
				'name' => 'Jose',
				'number' => '+51950119887'
			)
		);
	}
}